werbeo - commons-validators
---------------------------

A collection of java BeanValidation 1.1 (jsr-349) annotations and implementations.

Source
======
   * `@DOI`, `@DoiPrefix` and `@URL` are from [Datacite mds](https://github.com/datacite/mds)