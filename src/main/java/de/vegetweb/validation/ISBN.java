package de.vegetweb.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.*;

import javax.validation.*;

import org.apache.commons.validator.routines.ISBNValidator;

import de.vegetweb.validation.impl.ISBNConstraintValidator;

/**
 * Validates that a String is an ISBN-Number.
 * <p>
 * This is mainly a jsr-303 wrapper for {@link ISBNValidator}, but it accepts
 * empty strings and null as valid input.
 * 
 * @see ISBNValidator
 * @author dve
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { ISBNConstraintValidator.class })
public @interface ISBN {
    String message() default "{de.vegetweb.ISBN.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] value() default {};
}
