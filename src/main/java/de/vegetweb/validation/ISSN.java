package de.vegetweb.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.*;

import javax.validation.*;

import org.apache.commons.validator.routines.ISSNValidator;

import de.vegetweb.validation.impl.ISSNConstraintValidator;

/**
 * Validates that a String is an ISSN-Number.
 * <p>
 * This is mainly a jsr-303 wrapper for {@link ISSNValidator}, but it accepts
 * empty strings and null as valid input.
 * 
 * @see ISSNValidator
 * @author dve
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { ISSNConstraintValidator.class })
public @interface ISSN {
    String message() default "{de.vegetweb.ISSN.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] value() default {};
}
