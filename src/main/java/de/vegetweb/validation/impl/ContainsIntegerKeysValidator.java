package de.vegetweb.validation.impl;

import java.util.Map;

import javax.validation.*;

import de.vegetweb.validation.ContainsIntegerKeys;

public class ContainsIntegerKeysValidator
        implements ConstraintValidator<ContainsIntegerKeys, Map<Integer, ?>> {
    private int[] requiredKeys;

    public void initialize(ContainsIntegerKeys constraintAnnotation) {
        requiredKeys = constraintAnnotation.value();

    }

    public boolean isValid(Map<Integer, ?> value,
            ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        for (int requiredKey : requiredKeys) {
            if (!value.containsKey(requiredKey)) {
                return false;
            }
        }

        return true;
    }

}
