package de.vegetweb.validation.impl;

import javax.validation.*;

import org.apache.commons.validator.routines.ISBNValidator;

import de.vegetweb.validation.ISBN;

public class ISBNConstraintValidator
        implements ConstraintValidator<ISBN, String> {

    public void initialize(ISBN constraintAnnotation) {
        // nothing to initialize

    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        if ("".equals(value) || value == null) {
            return true;
        }
        return ISBNValidator.getInstance(false).isValid(value);
    }

}
