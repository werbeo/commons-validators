package de.vegetweb.validation.impl;

import javax.validation.*;

import org.apache.commons.validator.routines.ISSNValidator;

import de.vegetweb.validation.ISSN;

public class ISSNConstraintValidator implements ConstraintValidator<ISSN, String> {

    public void initialize(ISSN constraintAnnotation) {
        // nothing to initialize
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        if ("".equals(value) || value == null) {
            return true;
        }
        return ISSNValidator.getInstance().isValid(value);
    }

}
