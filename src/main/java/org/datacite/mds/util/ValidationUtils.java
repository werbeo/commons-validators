package org.datacite.mds.util;

import javax.validation.ConstraintValidatorContext;

/**
 * Util class with validation related static methods.
 */
public class ValidationUtils {

    /**
     * shortcut for building a constraint violation attached to a specific node
     * 
     * @param context
     *            ConstraintValidatorContext
     * @param message
     *            message of the violation (e.g. template to be evaluated)
     * @param node
     *            node to attach the violation to
     */
    public static void addConstraintViolation(
            ConstraintValidatorContext context, String message, String node) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addNode(node)
                .addConstraintViolation();
    }

    /**
     * shortcut for building a constraint violation
     * 
     * @param context
     *            ConstraintValidatorContext
     * @param message
     *            message of the violation (e.g. template to be evaluated)
     */
    public static void addConstraintViolation(
            ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation();
    }
}
