package de.vegetweb.validation.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestISBNConstraintValidator {
    private ISBNConstraintValidator validator = new ISBNConstraintValidator();

    @Test
    public void test_validISBN10() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertTrue(validator.isValid("013390069X", null));
    }

    @Test
    public void test_validISBN13_withSeperatorDash() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertTrue(validator.isValid("978-0-13-390079-8", null));
    }

    @Test
    public void test_validISBN13_withSeperatorSpace() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertTrue(validator.isValid("978 0 13 390079 8", null));
    }

    @Test
    public void test_validISBN13_withoutSeperator() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertTrue(validator.isValid("9780133900798", null));
    }
    @Test
    public void test_validISBN13_wrongChecksum() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertFalse(validator.isValid("9780133900799", null));
    }

    @Test
    public void test_ISBN10_wrongChecksum() {
        // The Java Language Specification, Java SE 8 Edition (Java Series)
        // (Java (Addison-Wesley))
        assertFalse(validator.isValid("0133900691", null));
    }
}
