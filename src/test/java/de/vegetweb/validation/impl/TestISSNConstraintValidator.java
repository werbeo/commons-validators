package de.vegetweb.validation.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestISSNConstraintValidator {
    private ISSNConstraintValidator validator = new ISSNConstraintValidator();

    @Test
    public void test_validISSN() {
        // Valid ISSN for "ix"
        assertTrue(validator.isValid("0935-9680", null));
    }

    @Test
    public void test_wrongChecksum() {
        // ISSN for "ix" with wrong checksum
        assertFalse(validator.isValid("0935-9681", null));
    }

    @Test
    public void test_emptyISSN() {
        assertTrue(validator.isValid("", null));
    }

    @Test
    public void test_null() {
        assertTrue(validator.isValid(null, null));
    }
}
