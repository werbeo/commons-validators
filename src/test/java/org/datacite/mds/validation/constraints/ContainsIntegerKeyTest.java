package org.datacite.mds.validation.constraints;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import de.vegetweb.validation.ContainsIntegerKeys;

public class ContainsIntegerKeyTest extends AbstractContraintsTest {
    @ContainsIntegerKeys({ 0 })
    Map<Integer, String> map;

    @Test
    public void test_null() {
        assertTrue(isValid(null));
    }

    @Test
    public void test_empty() {
        assertFalse(isValid(new HashMap<Integer, String>()));
    }

    @Test
    public void test_contains() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(0, "This should be there");
        assertTrue(isValid(map));
    }

    boolean isValid(Map<Integer, String> map) {
        this.map = map;
        return super.isValid(this, "map");
    }
}
